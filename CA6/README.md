# Class Assignment 1 Report

The source code for this assignment is located in the folder CA1/tut-basic


## 1. Analysis, Design and Implementation

A section dedicated to the description of the analysis, design and implementation of the requirements

Should follow a "tutorial" style (i.e., it should be possible to reproduce the assignment by following the instructions in the tutorial)

Should include a description of the steps used to achieve the requirements

Should include justifications for the options (when required)


## 2. Analysis of an Alternative

For this assignment is there an aterntiave tool for Git? Describe it and compare it ti Git

## 3. Implementation of the Alternative

Present the implementation of this class assignment using the git alternative
