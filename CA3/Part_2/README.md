# Class Assignment 3 - Part 2 Report

The source code for this assignment is located in two folders: the implementation in Oracle VM VirtualBox inside "class_assignment",
and the alternative implementation in Hyper-V inside the "alternative" folder.


## 1. Analysis, Design and Implementation

This first section describes the steps we need to take to create an Ubuntu virtual machine using Oracle VM VirtualBox.

### Create BitBucket issues

These issues are related to the implementation of Vagrant using Oracle VM Virtual Box and later Hyper-V:

* CA3 | Part 2 - Prepare CA3 Part 2 and alternative

* CA3 | Part 2 - change gradle application: add support for building war file

* CA3 | Part 2 - change gradle application: add support for H2 console

* CA3 | Part 2 - change gradle application: fix other issues

* CA3 | Part 2 - change gradle application: change application context path

* CA3 | Part 2 - change Vagrantfile and application properties for alternative with Hyper-V

* CA3 | Part 2 - Write Class Assignment 3 - Part 2 report

### Create CA structure

Firstly, we create the necessary folders inside CA3 and the current README.md file:

    mkdir Part_2
    cd Part_2
    touch README.md
    mkdir class_assignment  
    mkdir alternative

This means we have created folder Part_2 and inside the README.md file, as well as directories class_assignment and alternative.

Then we copy the folder containing the spring boot application developed for Part 2 of Class Assignment 2, as was requested, into
both class_assignment and alternative, as we are going to try to access the application using both Oracle VM Virtual Box and Hyper-V.

The folder containing the application is called "demo", and can be copied from the local git repository from its location in CA2/Part_2/.

### Install Vagrant

Vagrant can be installed through its website by accessing this [link](https://www.vagrantup.com/downloads).
We choose the binary download for Windows 64-bit.

By writing the following command we verify that Vagrant was successfully installed:

![Image](./images/vagrant_v.JPG "vagrant_v")

### Configure demo application

There are some alterations that need to be made on the demo application we are using for Part 2, so that it will work correctly with a 
virtualization tool.
These modifications are necessary since up until now, we were using the locahost to access the application on the web. Now, we
intend to access the application using a customized IP address. This means we have to use a web server, in this case we choose 
Apache Tomcat.

We followed the changes that were mentioned on the teacher's repository, by following its commit history.
We can summarize them as follows:

- Add support for building war file

In section "plugins" of the build.gradle file, we add a plugin that will enable the application to build the war file that 
is going to be deployed by the web server:

        id 'war'
On the "dependencies" section of the build.gradle file, we include the following dependency to the Spring application so that it starts
using Tomcat as the embedded servlet container.

        providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

- Add support for H2 console

In the applications.properties file inside demo/src/main/resources, we add the following properties:

        spring.data.rest.base-path=/api
        spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
        spring.datasource.driverClassName=org.h2.Driver
        spring.datasource.username=sa
        spring.datasource.password=
        spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
        spring.jpa.hibernate.ddl-auto=update
        spring.h2.console.enabled=true
        spring.h2.console.path=/h2-console
        spring.h2.console.settings.web-allow-others=true

- Change application context path

Again in application.properties we add:

    server.servlet.context-path=/demo-0.0.1-SNAPSHOT

By default, Spring boot applications are accessed by the root directory "/". We are customizing the context path so that the application
is accessed through the path "/demo-0.0.1-SNAPSHOT".

We also need to update this context path inside the app.js file inside demo/src/main/js/:

    client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
        this.setState({employees: response.entity._embedded.employees});
    });

- Fix path to CSS file

In index.html, inside directory demo/src/main/resources/templates, we need to fix the path to the CSS file:

    <link rel="stylesheet" href="main.css" />

### Configure Vagrantfile

The Vagrantfile starts by stating which box we use to configure Vagrant. Vagrant boxes consist in the package format for Vagrant environments.
We choose [envimation/ubuntu-xenial](https://app.vagrantup.com/envimation/boxes/ubuntu-xenial), which uses Ubuntu Xenial 64bit.

We are going to create two virtual machines, db to hold the H2 database and web to hold the application.
Therefore, the next step consists in configuring each virtual machine. We start by stating the common configurations which include, for instance,
installing Java 8 that is used by the application.

Then, for the db virtual machine, we download H2 and set the IP address of its private network, as well as the ports we need to use to access the H2 console
from the localhost, and to access the H2 server from it.

Finally, we configure the web virtual machine, also by configuring its private network IP address, its RAM memory, and by installing tools that are needed to
download and build the application, such as git, nodejs and tomcat.

We need to change these lines (70-71) to adapt to our repository. First we insert the link to our remote repository,
then we specify the path that the web virtual machine will have to take in order to access the application.

    git clone https://fas28@bitbucket.org/fas28/devops-20-21-1201763.git
    cd devops-20-21-1201763/CA3/Part_2/class_assignment/demo

In order for the clone operation to work, we had to make this BitBucket repository public instead of private (its initial 
configuration).

We provide the machine with further instructions to be run on the command line after downloading the remote repository. These instructions make the virtual machine
enter the folder of the application and builds it.

Next, we just have to correct line 75 which is also run by the web virtual machine, which consists in copying the generated
war file from the libs directory into the tomcat8 webapps directory. The war (web application archive) file contains the contents of a web application.
This means that the application will be deployed on the web by webserver tomcat8. The modification we need to make consists in replacing 
"basic" with "demo" in the war file name, as our application is called "demo".

    sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

### Run virtual machines and access application

Now that both the demo application and the Vagrantfile are correctly configured, we can start using vagrant by running the following command:

    vagrant up

This command basically runs the Vagrantfile and creates the virtual machines that are configured in it. 
The default provider for Vagrant is Oracle VM VirtualBox, therefore it does not need to be specified.

After running this command, the virtual machines are successfully created and running on Oracle VM VirtualBox, as can be seen in this image:

![Image](./images/web_db_virtualbox.JPG "web_db_virtualbox")

Now it is time to test the application on the web by using links to access the two virtual machines:

- Access the H2 console using localhost (http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console):

![Image](./images/virtualbox_localhost_db.JPG "virtualbox_localhost_db")

- Access the H2 console using the IP address (http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console):

![Image](./images/virtualbox_ip_db.JPG "virtualbox_ip_db")

- Access the web application using localhost (http://localhost:8080/demo-0.0.1-SNAPSHOT/):

![Image](./images/virtualbox_localhost_web.JPG "virtualbox_localhost_web")

- Access the web application using the IP address (http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/):

![Image](./images/virtualbox_ip_web.JPG "virtualbox_ip_web")


## 2. Analysis of an Alternative

Oracle VM VirtualBox is an open-source software owned by Oracle. Hyper-V is owned by Windows. 
They are both hypervisors, i.e. software that enables the creation and running of multiple virtual machines
with their own operating systems (guest operating systems). Virtual machines are then "guests" of the computer 
with the hardware, which is called the host.

Oracle VM VirtualBox is a type 2 hypervisor, which means that it is an application that runs on the operating system of the host. 
On the other hand, Hyper-V is a type 1 hypervisor, thus it runs directly on the host's hardware and is always on as long as the host 
is powered on. 
Oracle VM VirtualBox, however, can be opened and closed by the user.

One important difference between these hypervisors is that Oracle VM VirtualBox can be run on various operating systems (Windows, macOS, Linux, etc.), 
whilst Hyper-V can only be run on Windows 10 with editions Enterprise, Pro or Education.

This compatibility issue is also reflected on the number of guest OS each hypervisor supports: Oracle VM VirtualBox supports a [big list of guest OS](https://www.virtualbox.org/wiki/Guest_OSes),
while Hyper-V is only able to create virtual machines that have Windows, Linux, and FreeBSD.

Another key difference between the two is that Hyper-V only supports hardware-assisted virtualization, whereas Oracle VM VirtualBox enables both hardware-assisted
virtualization and software-virtualization.

Concerning the type of virtual disks that each hypervisor supports, it's worth noting that while Oracle VM VirtualBox is able to create VDI, VMDK, VHD and HDD disks,
Hyper-V can only support disks of types VHD and VHDX.

Both of the virtualization tools have a functionality that records the state of a virtual machine at a given moment, preserving its configurations so that it is 
possible to return to that previous state whenever necessary. In Oracle VM VirtualBox this feature is represented as snapshots, in Hyper-V as checkpoints.

**Vagrant**

Vagrant's default provider is Oracle VM VirtualBox, which represents an advantage comparing to Hyper-V.
There are many more boxes available, then, for Oracle VM VirtualBox than for Hyper-V.

Moreover, there are additional limitations when integrating Vagrant with Hyper-V considering network configurations.
Whilst it is possible to configure static IPs on the Vagrantfile for Oracle VM VirtualBox, in Hyper-V this type of configuration
is ignored and therefore it becomes less-straightforward to configure network settings.

This table summarizes the key differences and similarities between these two alternatives:

| Oracle VM VirtualBox      | Hyper-V |
| ----------- | ----------- |
| Type 2 hypervisor     | Type 1 hypervisor  |
| Free installation  | Free installation  |
| Runs on multiple OS   | Runs on Windows |
| Supports both hardware-assisted virtualization and software virtualization | Only supports hardware-assisted virtualization |
| Supports more guest OS | Supports less guest OS | 
| Supports more types of virtual disks | Supports less types of virtual disks |
| Snapshots | Checkpoints |
| Is the default provider of Vagrant | Limited integration with Vagrant |


## 3. Implementation of the Alternative

In this section we describe how to use Hyper-V, instead of Oracle VM VirtualBox, to deploy the web application of CA2 Part 2.

### Install Hyper-V

The first step consists in enabling Hyper-V. As was mentioned in the analysis of the alternative, Hyper-V is not compatible with 
Windows 10 Home, which is the edition we have. However, there is a workaround for this issue, which consists in running a bat file 
(which is written in bash), which will enable Hyper-V for Windows 10 Home.

Therefore, we run the Hyper-V enabler available on [this website] (https://www.itechtics.com/enable-hyper-v-windows-10-home/), and 
then we just need to open Hyper-V Manager.

### Configure Vagrantfile

In order to use Vagrant with Hyper-V, we have to use a different box, as the one used previously was not compatible with Hyper-V.

Therefore, in the Vagrantfile we added a new box ([tvinhas/ubuntu-16.04-hyperv](https://app.vagrantup.com/tvinhas/boxes/ubuntu-16.04-hyperv)) in the 
required sections:

    Vagrant.configure("2") do |config|
    config.vm.box = "tvinhas/ubuntu-16.04-hyperv"
    config.vm.synced_folder ".", "/vagrant", disabled: true

    config.vm.define "db" do |db|
    db.vm.box = "tvinhas/ubuntu-16.04-hyperv"

    config.vm.define "web" do |web|
    web.vm.box = "tvinhas/ubuntu-16.04-hyperv"

As this box requires a login, we choose to skip that step by adding the configuration shown in Vagrant.configure("2").

We also need to update the path that the web virtual machine takes to access the application:

    cd devops-20-21-1201763/CA3/Part_2/alternative/demo

As was explained in the analysis section, there is no need to change the IPs statically configured in Vagrantfile as these network configurations
are ignored when we use Hyper-V as the virtualization tool.

### Configure application properties

The next step consists running Vagrant so that the virtual machines are created. As now we are using a different provider from the default,
we have to specify it:

    vagrant up --provider hyperv

However, when we access the h2 console and the web application, we get a 404 Bad Request HTTP response. This is due to the fact that the IP address
we insert is not the one that was generated when Hyper-V launched the virtual machines.

There is, however, a way to fix this problem, by changing the Spring application properties instead of the Vagrantfile.
We should access the created web virtual machine using ssh:

    vagrant ssh web

We access the application.properties file through the following path:

    cd devops-20-21-1201763/CA3/Part_2/alternative/demo/src/main/resources

We then ask to edit the file:

    nano application.properties

Finally, we change the spring.datasource.url property, which specifies the IP address of the db virtual machine. We update the IP
address with the one that is shown in Hyper-V Manager:

![Image](./images/hyper_v_db_ip_address.JPG "hyper_v_db_ip_address.JPG")

We can see how the file looks after the alteration:

![Image](./images/nano_application_properties.JPG "nano_application_properties")

In order to apply these changes we run this command after exiting ssh:

    vagrant reload --provision

This command is equivalent to stopping (vagrant halt) and running the virtual machine again (vagrant up), and the "provision" flag
forces the provisioners to re-run.

### Run virtual machines and access application

In order to test the application and the H2 console on the web, we have to check the IP address of the web virtual machine:

![Image](./images/hyper_v_web_ip_address.JPG "hyper_v_web_ip_address")

Now we can access the links:

- Access the H2 login using the IP address (http://172.20.255.123:8080/demo-0.0.1-SNAPSHOT/h2-console):

![Image](./images/browser_ip_h2_console_working.JPG "browser_ip_h2_console_working")

Notice how we have to insert the db virtual machine IP address on the JDBC url.

- Access the H2 console using the IP address (by connecting on the H2 login):

![Image](./images/browser_ip_h2_working.JPG "browser_ip_h2_working")

- Access the web application using the IP address (http://172.20.255.123:8080/demo-0.0.1-SNAPSHOT/):

![Image](./images/browser_ip_web_working.JPG "browser_ip_web_working")

### Tag the repository with tag ca3-part2

At the end of part 2 of this assignment, we tag the repository with the tag "ca3-part2":

    git tag ca3-part2
    git push origin ca3-part2

The tag can be seen in the repository:

![Image](./images/git_tag_ca3part2.JPG "git_tag_ca3part2")








