# Class Assignment 3 - Part 1 Report

The source code for this assignment is located inside the virtual machine created that was created as a part of this assignment.

In order to complete the first part of this assignment I had to follow the steps detailed in this report.


## 1. Class Assignment 3 Preparation

**Create Virtual Machine using Oracle VM VirtualBox**

For the first part of the assignment we want to create a virtual machine using Oracle VM VirtualBox that runs with the Linux operating system.
The goal is to run the gradle application of Class Assignment 2 - Part 2 inside the virtual machine.

The first step consists in installing Oracle VM VirtualBox (https://www.virtualbox.org/), a free hypervisor that connects
a host operating system to multiple virtual machines.

Once installed, a series of steps are taken sequentially in order to configure the new virtual machine:

1. Click on "New" button in the GUI of Oracle VM VirtualBox.
2. Write the name of the virtual machine (e.g.: Ubuntu), select Linux as type and Ubuntu (64-bit) as version.
3. Choose RAM memory size (e.g.: 2048 MB).
4. Choose option "Create virtual hard disk now".
5. Choose hard disk file type "VDI (VirtualBox Disk Image)".
6. Choose option "Dynamically allocated" hard disk file.
7. Choose hard disk file location (e.g.: C:\Users\SWitCH\VirtualBox VMs\sffsf\sffsf.vdi) and size (e.g.: 10 GB).

**Configure Ubuntu Virtual Machine**

The virtual machine is now created, but some additional configurations are necessary:

8. Install Ubuntu on the virtual machine using an ISO file, i.e. a file that acts as a virtual installation cd. 
We are using an ISO image of Ubuntu 18.04 "Bionic Beaver" for 64-bit PC found in https://help.ubuntu.com/community/Installation/MinimalCD. 
   After downloading the ISO file, we go to the virtual machine's settings and inside the Storage section, upload the ISO file to the Optical Drive of
   Controller: IDE, and check the Live CD/DVD option.
9. Configure the virtual machine's networks by accessing its settings and the Network section. We select NAT as the Adapter 1 and a Host-only Adapter 
with name VirtualBox Host-Only Ethernet Adapter as Adapter 2.
10. Configure the host-only ethernet adapter by accessing File -> Host Network Manager, which will enable us to insert the IPv4 Address and mask (192.168.56.1/24).

After these configurations the virtual machine appears with the following definitions inside Oracle VM VirtualBox:

![Image](./images/vm_created_after_settings.JPG "vm_created")

**Install Ubuntu**

Now we can start the virtual machine and follow the instructions there to install the Ubuntu version of the Linux operating system.

**Additional configurations**

After successfully installing Ubuntu, we can log in to our newly created Ubuntu account and run additional Linux commands, which will make it possible
to access the virtual machine through a command line terminal in our host machine.

Therefore, we run the following commands to:

- Update the information about packages:
      
      sudo apt update

- Install network tools:
  
      sudo apt install net-tools
  
- Configure the network interfaces:

      sudo nano /etc/netplan/01-netcfg.yaml
  
And edit the file so that it contains this information:

      network:
         version: 2
         renderer: networkd
         ethernets:
            enp0s3:
               dhcp4: yes
            enp0s8:
               addresses:
                  - 192.168.56.5/24

- Apply changes that were made:
  
      sudo netplan apply
  
- Install openssh-server so that we can access the virtual machine from the host:

      sudo apt install openssh-server
  
- Enable password authentication in SSH by uncommenting the line "PasswordAuthentication yes" in this file:
  
      sudo nano /etc/ssh/sshd_config

And reload the ssh server so that this change is applied:

      sudo service ssh restart
  
- Install an ftp server so that we can share files between the host and the virtual machine using the FTP protocol:
  
      sudo apt install vsftpd
  
- Enable write access for vsftpd by uncommenting the line "write_enable=YES" in the file:
  
      sudo nano /etc/vsftpd.conf
  
And reload the vsftpd so as to apply this change:

      sudo service vsftpd restart

**Use SSH Server**

Since we have configured SSH Server on the virtual machine, we are able to access it using SSH from the host.

Therefore, from the command line in the host Windows, we can log in using the command structure:

    ssh <username>@<ip address of virtual machine>

In our case we have to write:

    ssh filipa@192.168.56.5

The following image shows we were successful:
![Image](./images/windows_ssh_login.JPG "windows_ssh_login")


## 2. Run the Spring application inside the Virtual Machine

Now that we have logged in the virtual machine through the host using SSH, we can try to run applications from previous class assignments.

The first step consists in downloading the applications from our remote git repository. Therefore, we need to install git on the virtual machine using the command:

    sudo apt install git

We also need to install Java 8 as the application is written in this language:

    sudo apt install openjdk-8-jdk-headless

Since we have installed git we can clone the git repository into our virtual machine:

![Image](./images/git_clone_my_repository.JPG "git_clone_my_repository")

**Run tut-basic application**

The tut-basic application is inside the Class Assignment 1, Part 1 folder.

When we try to run it for the first time, the following error appears, stating that we don't have permission to
execute the ./mvnw command:

![Image](./images/spring_tut_basic_permission_denied.JPG "spring_tut_basic_permission_denied")

This is surpassed by changing the permissions for mvnw, by granting an "execute" (x) permission:

![Image](./images/chmod.JPG "chmod")

We can check that everything is working by accessing the browser and typing the IP address defined for our virtual machine and port 8080:

![Image](./images/spring_browser_tut_basic.JPG "spring_browser_tut_basic")

All of the tests developed for this application are running correctly as well:

![Image](./images/spring_test.JPG "spring_test")

**Run gradle application**

The gradle basic demo application is inside the Class Assignment 2, Part 1 folder.

The first step consists in installing gradle so that the virtual machine recognizes its commands:

    sudo apt install gradle

Now we can successfully build the gradle application:

![Image](./images/gradle_build.JPG "gradle_build")

And execute task runServer:

![Image](./images/gradle_runServer.JPG "gradle_runServer")

Notice that we have to run task runClient inside our host machine, since the installed virtual machine has no GUI.
As we are using the host machine, we need to modify task runClient so that instead of using
localhost to communicate with the server, it connects with it using the virtual machine in which
the server is running. Therefore, instead of localhost we specify the virtual machine's IP address (192.168.56.5).

We have created a new task (runClient2) to achieve this inside the build.gradle file:

    task runClient2(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Launches a chat client that connects to a server on 192.168.56.5:59001 "

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatClientApp'

        args '192.168.56.5', '59001'
    }

We can see that the chat application is working well:

![Image](./images/gradlew_runClient.JPG "gradlew_runClient")

![Image](./images/chat_ana_joana.JPG "chat_ana_joana")


## 3. Tag the repository with tag ca3-part1

At the end of this part of the assignment we tag the repository with the tag "ca3-part1":

    git tag ca3-part1
    git push origin ca3-part1

The tag can be seen in the repository:

![Image](./images/git_tag_ca3part1.JPG "git_tag_ca3part1")