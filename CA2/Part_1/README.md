# Class Assignment 2 - Part 1 Report

The source code for this assignment is located in the folder CA2/Part_1/gradle_basic_demo.

In order to complete the first part of this assignment I had to follow the steps detailed in this report.


## 1. Class Assignment 2 Preparation

**Cloning**

I created a folder for Part 1 of this assignment:


    cd IdeaProjects/devops-20-21-1201763/CA2
    mkdir Part_1

I copied the README.md file in CA2 to Part_1, and then removed it from CA2:

    mv README.md Part_1/
    git rm --cached README.md

Finally, I cloned the remote repository containing the gradle application:

    cd Part_1
    git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git

**Gradle**

For this assignment I chose to install Gradle in my computer. Instead, I could have used
the gradle wrapper with which it's possible to run gradle commands without installing gradle,
or without having the same gradle version used by the application.

I installed Gradle following the instructions at the website https://gradle.org/install/.

By running the following command it's possible to check if gradle was successfully installed,
and with which version:

    gradle -v



## 2. Experiment the application

I followed the instructions of the README.md file of the gradle application in order to test it.

The following is the result of executing gradle build:

![Image](./images/gradle_build.JPG "build")

I also run the application inside IntelliJ:

![Image](./images/run_application.JPG "run")

Then it is necessary to run the server of the chat using this command:

    gradle runServer

Once the server is running, it's possible to launch the chat application. In order to do this, it's 
necessary to open a new command line window and run the command below. This  opens a chat window for a new user.

    gradle runClient

![Image](./images/gradle_runClient.JPG "runClient")

Whenever a new user wants to join the chat, a new command line window has to be opened and the command above has to be 
run again.

As people join and leave the chat, the chat server handler is updated as it is shown in the following image:

![Image](./images/chat_server_handler.JPG "handler")

The following image shows the result of the interaction in the chat between two distinct users:


![Image](./images/chat.JPG "chat")


## 3. Add a new task to execute the server

Tasks are written in the build.gradle file.
The runServer task is of JavaExec type, which executes a Java application (https://docs.gradle.org/current/dsl/org.gradle.api.tasks.JavaExec.html).

This task is similar to runClient. The "main" parameter had to be changed to ChatServerApp, and the "args" parameter as well in order to match the arguments accepted
by the main method of ChatServerApp.

    task runServer(type:JavaExec, dependsOn: classes){
        group = "DevOps"
        description = "Starts running the chat server on localhost:59001"

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatServerApp'

        args '59001'
    }

In command line, I run:

    gradle runServer

By running this command I got the following output, indicating it was successful:

![Image](./images/gradle_runServer.JPG "runServer")


## 4. Add a unit test

I had to add a new dependency to the build.gradle file, related to junit version 4.12, which consists in
the last line in the dependencies section:

    dependencies {
        // Use Apache Log4J for logging
        implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        testImplementation 'junit:junit:4.12'
        }

I had to create a tests folder in src and generate an AppTest class to test the getGreeting() method in class
App. Inside the AppTest I copied the code from the class assignment file:

    package basic_demo;

    import org.junit.Test;
    import static org.junit.Assert.*;

    public class AppTest {
    
        @Test public void testAppHasAGreeting() {
            App classUnderTest = new App();
            assertNotNull("app should have a greeting", classUnderTest.getGreeting());
        }
    }

The test was successfully run at IntelliJ:

![Image](./images/gradle_test_passed.JPG "test_passed")

It can also be run by the command line using the command below, which runs all unit
tests

![Image](./images/gradle_clean_test.JPG "test_passed")

The command "gradle test" could have been used instead, however in this case the output from
previous builds wouldn't have been cleaned.


## 5. Add a new task to backup the sources of the application

The task type is Copy, and the necessary parameters to be specified are "from" and "into"
(https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Copy.html).

In the build.gradle file, I wrote:

    task backupSrc(type:Copy, dependsOn: classes) {
        group = "Devops"
        description = "Creates a backup of the src folder"

        from 'src/'
        into 'backup/'
    }

In command line, I run:

    gradle backupSrc

This command generates a successful result:

![Image](./images/gradle_backupSrc.JPG "backupSrc")


## 6. Add a new task to zip the sources of the application

The task type is Zip, and the necessary parameters to be specified are "from", "archiveName" and
"destinationDir" (https://docs.gradle.org/current/dsl/org.gradle.api.tasks.bundling.Zip.html).

In the build.gradle file, I wrote:

    task zipSrc(type:Zip, dependsOn: classes) {
        group = "Devops"
        description = "Creates a zip file of the src folder"

        from 'src/'
        archiveName 'backup.zip'
        destinationDir(file('backupZip/'))
    }

In command line, I run:

    gradle zipSrc

This command generates a successful result, however with a deprecation warning:

![Image](./images/gradle_zipSrc.JPG "zipSrc")

Now that backupSrc and zipSrc tasks have been run, it's possible to see that the corresponding folders and file were created:

![Image](./images/project_structure_folders.jpg "folders")


## 7. Tag the repository with tag ca2-part1

In command line, I run:

    git tag ca2-part1
    git push origin master ca2-part1

The tag now appears associated with this commit:

![Image](./images/git_tag.JPG "tag")


