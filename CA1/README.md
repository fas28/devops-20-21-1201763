# Class Assignment 1 Report

The source code for this assignment is located in the folder CA1/tut-basic.

The git and hg commands exemplified in this report follow this sequence whenever it's appropriate: first, I introduce a generic command 
that explains what must be written in each part; then, I write the command I have used in a specific case in order
to illustrate it.


## 1. Analysis, Design and Implementation

This first section of the report explains the steps taken to implement Class Assignment 1, related 
to the use of the distributed version control system Git.

### 1.1 Create BitBucket issues

In this tutorial I have used BitBucket as the service to host the remote git repository.
BitBucket allows the creation of issues which are useful to plan the workflow of this project.
Commits can be automatically associated with issues, therefore making the progress visible in a timeline style.

I have created these issues on BitBucket:

* Write a README.MD file to specify the steps taken in CA1
* Create folders for class assignments with corresponding README.md files
* Add tut-basic folder to CA1 which corresponds to v1.2.0
* Email-field feature: create a branch
* Email-field feature: insert this field and validation methods
* Email-field feature: insert validation methods for other Employee fields
* Email-field feature: add tests to validate all Employee fields which corresponds to v1.3.0
* Fix bugs of the feature "email-field": create branch and validation method
* Fix bugs of the feature "email-field": test validation method wich corresponds to v1.3.1

This tutorial report is structured according to these issues, as they reflect the workflow.
The first issue will not be included in this report since it refers to the report itself.

### 1.2 Create folders for class assignments with corresponding README.md files

I have used mostly Git Bash command line to do this assignment.
In the command line I have written this command to create the folders with corresponding README.md files (one
command is here represented as an example, the first CA folder):

    Generic:
    mkdir <folder name>
    cd <folder name>
    touch <file name>

    Commands used:
    mkdir CA1/
    cd CA1/
    touch README.md

The first line creates the folder (directory), the second moves inside the created folder, and the third creates
the file README.md.
The next step consists in adding the folder and its file to the staging area, make a commit and push it to the remote repository.

    Generic:
    git add <files or . for everything that's been modified>
    git commit -m "<commit message, which is optional>"
    git push <remote> <branch>
    
    Commands used:
    git add CA1/README.md
    git commit -m "Holds issue #4: added folder CA1 with corresponding README.md file"
    git push origin master

The first command adds the file to the staging area, the second commits it, and the third pushes it to the master branch in the remote
repository origin.
Then the same sequence of commands is repeated for the remaining folders.

This is the way that git works: it detects the modified versions of a file which we can add to the staging area using the git add command;
then we can commit those staged files to our local repository; and finally, we're able to push this local commit to a remote repository.

### 1.3 Add tut-basic folder to CA1 which corresponds to v1.2.0

The Tutorial React.js and Spring Data REST application that was used in this assignment is the one that resulted from  the
tutorial exercise made on 11-03-2021. It was placed in the remote repository within folder tutorial/tut-basic. The application
itself was inside the tut-basic folder. 
Therefore, it was necessary to create a copy of it inside the CA1 folder. 
I have used the following command to copy the folder to a new destination:

    Generic:
    cp -r <source directory> <destination directory>

    Commands used:
    cp -r tutorial/tut-basic CA1/

After adding this folder I had to tag this version of the application as v1.2.0. To achieve this I created a git tag and pushed 
it to the git remote repository. It will then be automatically associated with the last commit.

    Generic
    git tag <tag name>
    git push <remote> <tag name>

    Commands used:
    git tag v1.2.0
    git push origin v1.2.0

In case it was necessary to associate the tag with another commit, one would only have to write a command line like the following:

    Generic
    git tag <tag name> <commit sha> 
    git push <remote> <tag name>

###  1.4 Email-field feature: create a branch

In order to develop a new feature in the application I have created a branch called "email-feature".
Branches allow working on a separate path from the master branch. This presents many advantages, for instance it's
possible for different elements of a team to work on different features without resulting in conflicting versions of the 
same application. Branches are useful to clearly identify and track the work that's being made on the many features, and once 
these features are ready to be implemented they can be merged with the master branch, which will only contain the final versions
of each feature, thus resulting in a clearer path.

The following commands create the "email-feature" branch (first command) and move the HEAD of repository to that branch (second 
command), the HEAD being a pointer that points to the current branch.

    Generic:
    git branch <branch name>
    git checkout <branch name that we want to move into>
    
    Commands used:
    git branch email-feature
    git checkout email-feature
    
We can also use this command to view all the branches in our repository and the last commit on each one. The asterisk symbol
signals the branch we're currently in.

    git branch -v

### 1.5 Email-field feature: insert this field and validation methods

Since I've created and moved to the new branch "email-field", I had to start implementing the email feature, which implied
changes to different files in the application, both at the back-end (Employee and DatabaseLoader) and the front-end (app.js).

* In Employee java class:

*Changed constructor by including the field email:*

	public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
		setFirstName(firstName);
		setLastName(lastName);
		setDescription(description);
		setJobTitle(jobTitle);
		setEmail(email);
	}

*Changed equals and hashCode methods by including the field email:*

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, email);
	}

*Added methods getEmail, setEmail and validateEmail:*

The validation consisted in not allowing null or empty values for the email attribute. Otherwise, an Illegal Argument Exception
is thrown.

	public String getEmail() {return email; }

	public void setEmail(String email) {
		if (!validateEmail(email)) {
			throw new IllegalArgumentException("Email cannot be empty or null or have an invalid format");
		}
		this.email = email;
	}

    private boolean validateEmail(String email) {
        return email != null && !email.equals("") && validateEmailFormat(email);
    }

* In DatabaseLoader java class:

I had to insert a new field for email in each line.

    public void run(String... strings) throws Exception { // <4>
        this.repository.save(new Employee("Frodo", "Baggins", "ring bearer","ring hunter", "frodo_baggins@hobbits.com"));
        this.repository.save(new Employee("Snow", "White", "princess", "apple taster", "snow_white@disney.com"));
    }

* In app.js:
  
I had to add the email field as well in these methods.

    class EmployeeList extends React.Component{ render() {
        const employees = this.props.employees.map(employee =>
        <Employee key={employee._links.self.href} employee={employee}/>
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Description</th>
                    <th>Job</th>
                    <th>Email</th>
                </tr>
                {employees}
                </tbody>
            </table>
                )
            }
        }

    class Employee extends React.Component{
        render() {
            return (
                <tr>
                    <td>{this.props.employee.firstName}</td>
                    <td>{this.props.employee.lastName}</td>
                    <td>{this.props.employee.description}</td>
                    <td>{this.props.employee.jobTitle}</td>
                    <td>{this.props.employee.email}</td>
                </tr>
                )
                }
        }

After these changes I had to repeat the git sequence of adding, committing and pushing the modified files to the
remote repository. There's an important difference this time since I had to push to the "email-feature" branch, 
as it is represented next:

    git push origin email-feature

### 1.6 Email-field feature: insert validation methods for other Employee fields

It was part of the assignment to do the validation of the other Employee fields.
These validation methods followed the same structure of the validation method for the email, therefore an example 
is not included in this section.

### 1.7 Email-field feature: add tests to validate all Employee fields which corresponds to v1.3.0

I added unit tests for the Employee constructor and for the validation methods of the Employee fields.
Both successful and unsuccessful scenarios were tested.
As an example I place here a tests for the Employee constructor and the setEmail methods:

        @Test
        void EmployeeTestConstructor_ValidEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(email,employee.getEmail());

    }

    @Test
    void setEmployeeEmail_EmptyEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

After writing and running successfully the tests, I had to merge the "email-feature" branch with the master branch.
In order to do so, I had first to move into the master branch and then merge it with the other branch.
    
    Generic:
    git checkout master
    git merge <branch we want to merge>

    Commands used:
    git checkout master    
    git merge email-feature

This merge consisted in a fast-forward merge, in which Git "fast-forwards" the current branch tip up to the target branch tip, i.e., 
Git moved its HEAD pointer to the merged branch tip. This happened because no work had been done on the master branch at the same
time as work was being done on the "email-feature" branch.

The output of this merge consisted in version v1.3.0 of the application. Therefore, I wrote the following commands:

    git tag v1.3.0
    git push origin v1.3.0

### 1.8 Fix bugs of the feature "email-field": create branch and validation method

The workflow of this step is similar to the workflow of the "email-feature" branch, this time with a branch named "fix-invalid-email" which
is supposed to fix some bugs related to invalid email formats.

The first step consisted in creating a new branch:

    git branch fix-invalid-email
    git checkout fix-invalid-email

Then some small changes had to be made on Employee java class in order to further validate the email format.
The validateEmail method had to contain an additional validation, which is related to a new method called validateEmailFormat.

    private boolean validateEmail(String email) {
        return email != null && !email.equals("") && validateEmailFormat(email);
    }

	private boolean validateEmailFormat(String email) {
		// Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
				"[a-zA-Z0-9_+&*-]+)*@" +
				"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
				"A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		return pat.matcher(email).matches();

	}

### 1.9 Fix bugs of the feature "email-field": test validation method which corresponds to v1.3.1

This step consisted in adding unit tests to assess the success of the validateEmailFormat method. Both successful and 
unsuccessful scenarios were tested. 

I present here an example of the tests:

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_PrefixWithInvalidCharacter() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow#white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

Finally, this "fix-invalid-email" branch had to be merged with the master branch, and then tagged as v1.3.1, following a similar 
sequence:

    git checkout master    
    git merge fix-invalid-email

    git tag v1.3.1
    git push origin v1.3.1

We can see in the following image how a fast-forward merge is presented in the command line:

![Image](./tut-basic/images/git%20merge%20fix-invalid-email.JPG "git merge")

Contrary to merges that require other merging strategies, the fast-forward strategy doesn't lead to a merge commit.

### 1.10 Debug client and server parts

In order to debug the client side of the application, I had to run the Spring application from the terminal using the following command
on the tut-basic folder, where the application is located:

    ./mvnw spring-boot:run

The two final lines after running this command indicate that the application was successfully launched:

![Image](./tut-basic/images/spring%20run.JPG "spring")

We can see that it is working on the front-end side by accessing localhost:8080 on a web browser and seeing the table:

![Image](./tut-basic/images/localhost.JPG "localhost")

On the server side, we can see that everything is working by running all tests of the Employee java class and checking
that they're all successful:

![Image](./tut-basic/images/tests%20git.jpg "tests git")

### 1.11 Observations

At some point in this class assignment I chose to start all over again in order to follow a different sequence of steps and 
to prevent some mistakes I had made previously. Therefore, the initial commit to take into account is the one with
the SHA code 36e5b98, from 03-04-2021.

I noticed at some point that my local branches ("email-feature" and "fix-invalid-email") were not associated with any remote branches.
Therefore, I had to run the following commands in order to associate them:

    Generic:
    git checkout <branch you wish to track>
    git push -u <remote> <branch name>

    Commands used:
    git checkout fix-invalid-email
    git push -u origin fix-invalid-email

    git checkout email-feature
    git push -u origin email-feature


## 2. Analysis of an Alternative

In this section I explain the main differences between Git and Mercurial, the alternative system I chose.

Both Git and Mercurial are Distributed Version Control Systems (DVCS) created in 2005; however, Git is considered the 
leading system in the market and Mercurial has been losing market share over the years.

DVCS, contrary to centralized approaches to version control, allow each collaborator to mirror the code and version
history of the project in their own local repositories. In centralized version control there is no concept of local repository,
projects are centralized in a single server.

The following subsections explain the main differences between Git and Mercurial.

### 2.1 Staging

Staging is a process in which you can add files for your next commit, so it may be useful when we don't want to include
all modified files in the same commit.
There is no staging area before each commit in Mercurial by default. However, some Mercurial extensions can be installed 
to perform this step.
There is a staging area in Git, to which you can send files using the command:

    git add <folder of file>

In Mercurial, the "add" command holds a different meaning. The following command is used when you wish to track previously untracked
files in the Mercurial remote repository:

    hg add <folder or file>

After tracking these files, everytime they are modified they are detected as modified by Mercurial, and therefore included in the next commit.
This way, it's not possible to select which files to commit through a staging area.

### 2.2 Branching

Branching is considered to be more effective in Git. In Git, a branch is a pointer to a certain commit, . 
In Mercurial, a branch refers to a sequence of changesets, i.e. a set of changes made to a file. This means that a branch
is embedded in the commits, therefore they can't be removed because it would have to rewrite the version history.
In Git, a branch can be easily created, altered and deleted without affecting the version history.

In Mercurial, besides branches there are bookmarks, which resemble git branches more closely in the way they function. 
A bookmark references to a specific commit, like a git branch. 

The main branch in Git is named "master", whereas in Mercurial it is called "default".

### 2.3 Other differences

The following is a list of additional differences between these two DVCS:

* Git is more complex and better for larger teams and projects;
  
* Mercurial is better suited to less-experienced developers and smaller teams;
  
* Mercurial's commands and documentation are simpler and easier to understand;  
  
* Git requires periodic maintenance of repositories, Mercurial doesn't.

## 3. Implementation of the Alternative

The changes that need to be applied to the application are the same in this alternative, and as they were already described in 
section 1 of the report, they won't be explained again in this section.

Therefore, on this section of the report I choose to describe the differences related to the implementation of another distributed
version control system, i.e. Mercurial instead of Git.

### 3.1 HelixHub

I chose HelixHub as the service to host the remote Mercurial repository, as BitBucket no longer supports this type of repository.
HelixHub also allows the creation of issues, which are fit into milestones.

Inside the milestone Develop the feature "email-field" I created these issues:

* Create the branch to develop this feature
  
* Add tut-basic to repository
  
* Add .hgignore file to repository
  
* Configure hgrc file
  
* Create email attribute and its validation
  
* Create validation methods for other attributes
  
* Create tests for all Employee attributes
  
* Merge branch email-feature with default

Inside the milestone Fix bugs in feature "email-field" I created these issues:

* Create the branch to develop this feature
  
* Create method to validate email format
  
* Create tests to test the validation of the email format
  
* Merge branch fix-invalid-email with default

### 3.2 Hg in the command line

#### 3.2.1 Create a branch and push it to remote repository:

In Mercurial, in order to create and push a new branch to a remote repository, the following commands are written:
    
    Generic:
    hg branch <branch name>
    hg push --new-branch

    Commands used (example):
    hg branch email-feature
    hg push --new-branch

It's relevant no notice that, contrary to Git, pushing branches requires writing "--new-branch" after the push word.

#### 3.2.2 Add, commit and push files to remote repository:
    
When adding a file/directory for the first time:

    Generic:
    hg add <file>
    hg commit -m "<optional message>"
    hg push
    
    Commands used (example):
    hg add .hgignore
    hg commit -m "Resolving #3: added file .hgignore"
    hg push 

When working on a file that was already added:

    Generic:
    hg commit -m "<optional message>"
    hg push
    
    Commands used (example):
    hg commit -m "WIP issue #5 altered method setEmail and added method validateEmail in Employee class"
    hg push

#### 3.2.3 Merge branch with the default branch:

The following commands are used to merge a branch with the default branch:

    Generic:
    hg check default
    hg merge <branch name>

    Commands used (example): 
    hg check default
    hg merge email-feature

#### 3.2.4 Create a tag:

In order to create a tag, the following commands are necessary:

    Generic:
    hg tag <tag name>
    hg commit -m "<optional message>"
    hg push

    Commands used (example):
    hg tag v1.3.0
    hg commit -m "Added tag v1.3.0 for changeset e05f7aadee87"
    hg push

Concerning tags, it's important to notice that once they are created, Mercurial automatically creates a file named .hgtags inside the
root directory. This file contains the following information:

    fc9b4bf4aac717f8f50c190c9acc76e21c893e59 v1.2.0
    e05f7aadee872558c28e9751173226709f19ca24 v1.3.0
    8dd76358af22c3e572f26cc156af62367e7e1a62 v1.3.1
    3af2d32bb64a69e6dc6beeb9292b78c3ec1d9cd3 ca1

### 3.3 TortoiseHg

TortoiseHg is a Windows shell extension that is a GUI front-end for Mercurial. Although I have used the command line 
to implement the steps of this assignment, I could have used the buttons of this GUI to perform the same steps. 

At the end of this assignment I was able to see a graphical representation of the workflow in TortoiseHg:

![Image](./tut-basic/images/tortoise%20branches.JPG "tortoise branches")

### 3.4 Debug client and server parts

The debugging process is the same as the one implemented for Git, therefore its explanation won't be repeated in this section.
The only difference is that the Spring application had to be run in a different folder, the one where the Mercurial local repository
is located.

The following image demonstrates that tests were successfully run on the Mercurial repository:

![Image](./tut-basic/images/tests%20hg.jpg "tests hg")